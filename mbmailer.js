/**
 * Created by ivocazemier on 07/08/15.
 */

var async = require('async');
var path = require('path');
var mbMailerConfig = require('./lib/mbmailerconfig');

var MBMailer = require('./lib/mbmailer').MBMailer;
var QUEUE_EVENTS = require('./lib/mbmailer').QUEUE_EVENTS;

var MBMandrillWorker = require('./lib/mandrill/mbmandrillworker').MBMandrillWorker;
var MBMandrillMessage = require('./lib/mandrill/mbmandrillmessage').MBMandrillMessage;
var MBMandrillRecipient = require('./lib/mandrill/mbmandrillrecipient').MBMandrillRecipient;
var MBMandrillAttachment = require('./lib/mandrill/mbmandrillattachment').MBMandrillAttachment;

var MBSendGridWorker = require('./lib/sendgrid/mbsendgridworker').MBSendGridWorker;
var MBSendGridMessage = require('./lib/sendgrid/mbsendgridmessage').MBSendGridMessage;
var MBSendGridRecipient = require('./lib/sendgrid/mbsendgridrecipient').MBSendGridRecipient;
var MBSendGridAttachment = require('./lib/sendgrid/mbsendgridattachment').MBSendGridAttachment;

//Add more here later:


var CLASS_TYPES = {

    MANDRILL: 'mandrill',
    SENDGRID: 'sendgrid'

};


/**
 * Singleton instance mailer (lazy creation)
 */
var mailerInstance;
var getMailerInstance = function () {
    // Initialize singleton:
    return mailerInstance || (mailerInstance = new MBMailer());
};


/**
 * Mandrill Singleton instance for worker (lazy creation)
 */
var mandrillWorkerInstance;
var getMandrillWorker = function () {
    // Initialize singleton:
    return mandrillWorkerInstance || (mandrillWorkerInstance = new MBMandrillWorker());
};

/**
 * Mandrill Singleton instance for worker (lazy creation)
 */
var sendGridWorkerInstance;
var getSendGridWorker = function () {
    // Initialize singleton:
    return sendGridWorkerInstance || (sendGridWorkerInstance = new MBSendGridWorker());
};

/**
 * Expose interface for this module
 */
module.exports = {
    QUEUE_EVENTS: QUEUE_EVENTS,
    CLASS_TYPES: CLASS_TYPES,
    getMBMailerInstance: function(){
        /**
         * This is to expose the EventEmitter
         * (When a queuing system is used, this will not be needed anymore)
         */
        return getMailerInstance();
    },


    /**
     __  __       _ _
     |  \/  | __ _(_) | ___ _ __
     | |\/| |/ _` | | |/ _ \ '__|
     | |  | | (_| | | |  __/ |
     |_|  |_|\__,_|_|_|\___|_|

     ___       _             __
     |_ _|_ __ | |_ ___ _ __ / _| __ _  ___ ___
     | || '_ \| __/ _ \ '__| |_ / _` |/ __/ _ \
     | || | | | ||  __/ |  |  _| (_| | (_|  __/
     |___|_| |_|\__\___|_|  |_|  \__,_|\___\___|

     */
    sendMailWithMessage: function (mbMessage, callback) {
        var mailerInstance = getMailerInstance();
        mailerInstance.sendMailWithMessage(mbMessage, callback);
    },
    getClassesFactory: function (type) {

        /**
         * Expose all classes needed when one needs Mandrill (or later something else):
         */

        switch (type) {

            case CLASS_TYPES.MANDRILL:

                return {
                    MessageClass: MBMandrillMessage,
                    RecipientClass: MBMandrillRecipient,
                    AttachmentClass: MBMandrillAttachment
                };


                break;

            case CLASS_TYPES.SENDGRID:

                return {
                    MessageClass:MBSendGridMessage,
                    RecipientClass:MBSendGridRecipient,
                    AttachmentClass:MBSendGridAttachment
                };


                break;

            default:
                throw new Error("Unhandled type");
        }

    },
    /**
     __        _____  ____  _  _______ ____
     \ \      / / _ \|  _ \| |/ / ____|  _ \
     \ \ /\ / / | | | |_) | ' /|  _| | |_) |
     \ V  V /| |_| |  _ <| . \| |___|  _ <
     \_/\_/  \___/|_| \_\_|\_\_____|_| \_\

     ___ _   _ _____ _____ ____  _____ _    ____ _____
     |_ _| \ | |_   _| ____|  _ \|  ___/ \  / ___| ____|
     | ||  \| | | | |  _| | |_) | |_ / _ \| |   |  _|
     | || |\  | | | | |___|  _ <|  _/ ___ \ |___| |___
     |___|_| \_| |_| |_____|_| \_\_|/_/   \_\____|_____|


     */
    initializeWorker: function (appRootPath, callback) {
        var appRoot = path.resolve(appRootPath);
        if (appRoot) {
            mbMailerConfig.loadConfig(appRoot, function (error, settings) {
                if(!error) {
                    async.parallel(
                        [
                            function (callback) {
                                if (settings.mandrill) {
                                    var mbMandrillWorker = getMandrillWorker();
                                    mbMandrillWorker.initialize(settings.mandrill, callback);
                                } else {
                                    return callback(null);
                                }
                            },
                            function (callback) {
                                if (settings.sendgrid) {
                                    var mbSendGridWorker = getSendGridWorker();
                                    mbSendGridWorker.initialize(settings.sendgrid, callback);
                                } else {
                                    return callback(null);
                                }
                            }
                        ], function (error, results) {
                            return callback(error);
                        });
                } else {
                    return callback(error);
                }
            });
        } else {
            return callback(new Error('Root folder not found'))
        }
    },
    sendMailWithWorker: function (mbMessage, callback) {
        console.log('tset');
        if (mbMessage instanceof MBMandrillMessage) {

            console.log('tset');

            var mbMandrillWorker = getMandrillWorker();
            mbMandrillWorker.sendMailWithMessage(mbMessage, callback);

        } else if(mbMessage instanceof MBSendGridMessage) {
            console.log('tset');
            var mbSendGridWorker = getSendGridWorker();
            mbSendGridWorker.sendMailWithMessage(mbMessage, function(error) {
                callback(error);
            });

        } else {
            return callback(new Error("Unhandled message type"));
        }

    },
    degradeWorkerGracefully: function (callback) {
        async.parallel(
            [
                function(callback) {
                    var mbMandrillWorker = getMandrillWorker();
                    mbMandrillWorker.degradeWorkerGracefully(callback);
                },
                function(callback) {
                    var mbSendGridWorker = getSendGridWorker();
                    mbSendGridWorker.degradeWorkerGracefully(callback);

                }
            ], function(error, results) {
                callback(error);
            });
    }


};