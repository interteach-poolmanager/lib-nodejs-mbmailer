/**
 * Created by ivocazemier on 10/08/15.
 *
 * These config settings override the default settings for this module
 *
 *
 * This is an example config file, which needs to be at: [root app]/config/mail.js
 * (Is specific file is also used in the test.js)
 *
 *
 */



module.exports = {


    /**
     * Mandrill specific settings:
     * See: https://mandrillapp.com/api/docs/messages.nodejs.html
     */
    //Activate for using mandrill
    //mandrill: {
    //
    //    apiKey: process.env.MANDRILL_APIKEY || null,
    //    async: false,
    //    ip_pool: "Main Pool"
    //
    //},

    /**
     * Default sendgrid settings (authentication is done by username and password or by api key, only one of both authentication methods is needed)
     * userName: sendgrid account username
     * password: sendgrid account password
     * apiKey: sendgrid api key
     */
    //Activate for using sendgrid
    //sendgrid: {
    //    userName: process.env.SENDGRID_USERNAME || null,
    //    password: process.env.SENDGRID_PASSWORD || null,
    //    accessKey: process.env.SENDGRID_APIKEY || null
    //}
};