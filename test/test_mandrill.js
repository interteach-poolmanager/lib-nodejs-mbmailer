/**
 * Created by ivocazemier on 13/08/15.
 *
 *
 * Test works as follows:
 * - Add a listener to the mailer (which delegates the message to the worker)
 * - First initialize the worker (ready for sending mails)
 * - When worker is ready initializing, we will simulate in sending an email
 *
 * Prerequisites:
 * - Set environment variable: MANDRILL_APIKEY => SOME_SECRET_API_KEY
 *
 * Third Party Dependencies (see enclosed package.json):
 * - async (https://www.npmjs.com/package/async)
 * - request (https://www.npmjs.com/package/request)
 * - lodash (https://www.npmjs.com/package/lodash)
 * - file-type (https://www.npmjs.com/package/file-type)
 * - mandrill-api (https://www.npmjs.com/package/mandrill-api)
 */

// Native node module:
var path = require('path');

// Fetch path to current working directory (important for config file loading)
var cwd = path.resolve(__dirname);

// Fetch both independent modules (mailer + mailerworker)
var MBMailer = require('../mbmailer');


// Fetch classes for Mandrill:
var mandrillClasses = MBMailer.getClassesFactory(MBMailer.CLASS_TYPES.MANDRILL);
var MBMandrillMessage = mandrillClasses.MessageClass;
var MBMandrillRecipient = mandrillClasses.RecipientClass;
var MBMandrillAttachment = mandrillClasses.AttachmentClass;

console.log(mandrillClasses);
// Corresponding events:
var QUEUE_EVENTS = MBMailer.QUEUE_EVENTS;

// We need the instance, because it's an EventEmitter class
var mailerInstance = MBMailer.getMBMailerInstance();


/**
 * Create the listener on the mailer (listen for new jobs from the virtual queue)
 *
 * Simulate the Queueing system here, (normally the worker instance is able to listen for jobs from the queue all by itself)
 * When a event arises the event is delegated to the worker and is the only place where they are connected.
 * (Loosely coupled by event, not hard referenced)
 *
 */
mailerInstance.on(QUEUE_EVENTS.ENQUEUE_MESSAGE, function (mbMessage) {

    MBMailer.sendMailWithWorker(mbMessage, function (error, result) {

        if (error) {
            console.log(error);
        } else {
            console.log("YESS!");
        }

    });

});


/**
 * Bootstrap the mailer worker
 * And when ready we simulate the sending of an email
 * TEST!
 */
MBMailer.initializeWorker(cwd, function (error, result) {
    if (error) {
        console.log(error);
    } else {

        console.log(result);


        // Mailing system initialized:

        // Prepare a message for mandrill:

        var testMessage = new MBMandrillMessage();

        testMessage.fromEmail = "info@mediabunker.com";
        testMessage.fromName = "Ivo Cazemier";
        testMessage.important = true;

        ///////////////
        // Create recipients:
        var recipient1 = new MBMandrillRecipient();
        recipient1.email = "marco@mediabunker.com";
        recipient1.name = "Brute Bouwer.nl";
        recipient1.type = MBMandrillRecipient.RECIPIENT_TYPES.TO;

        //var recipient2 = new MBMandrillRecipient();
        //recipient2.email = "info@flashinnovations.nl";
        //recipient2.name = "Spam bucket";
        //recipient2.type = MBMandrillRecipient.RECIPIENT_TYPES.TO;
        //
        //var recipient3 = new MBMandrillRecipient();
        //recipient3.email = "admin@flashinnovations.nl";
        //recipient3.name = "administrator";
        //recipient3.type = MBMandrillRecipient.RECIPIENT_TYPES.BCC;
        //
        //////////////


        testMessage.recipients = [recipient1];//, recipient2, recipient3];


        ////////////////////////
        // Add attachments

        var attachment1 = new MBMandrillAttachment();
        attachment1.source = "http://www.gravatar.com/avatar/830bc62c075820dbd43f341e65b40093?s=200";
        attachment1.fileName = "gek.pdf";

        //http://vignette1.wikia.nocookie.net/mst3k/images/e/ee/RiffTrax-_Arnold_Schwarzenegger_in_Predator.jpg/revision/latest?cb=20140628084137
        var attachment2 = new MBMandrillAttachment();
        attachment2.source = "http://vvvvvvvvvignette1.wikia.nocookie.net/mst3k/images/e/ee/RiffTrax-_Arnold_Schwarzenegger_in_Predator.jpg/revision/latest?cb=20140628084137";
        //attachment2.source = "http://kjdsfbgkladsfbgafd.wikia.nocookie.net/mst3k/images/e/ee/RiffTrax-_Arnold_Schwarzenegger_in_Predator.jpg/revision/latest?cb=20140628084137";


        testMessage.attachments = [attachment1, attachment2];


        testMessage.subject = "Test subject!";
        testMessage.text = "Hello, this is a mail sent from my mailer code through mandrill";
        testMessage.html = "\
                    <hmtl>\
                        <body>\
                        <h2>Hello, this is a mail sent from my mailer code through mandrill</h2>\
                        </body>\
                    </html>\
                    ";

        console.log('test');

        MBMailer.sendMailWithMessage(testMessage, function (error, result) {
            console.log('test');
            if (error) {
                console.log(error);
            } else {
                console.log(result);
            }
        });


    }
});




