# MBMailer
## Release notes
### Version 0.1.0
- Sendgrid added
- Bugfix for reading non existing urls in mandrill

# README #

### What is this repository for? ###

* Abstract mailing with embedded attachments (Featuring Mandrill and Sendgrid api)

### How do I get set up? ###

* Ensure it's in your project
* npm install --save git+https://mediabunker:[secret_key]@bitbucket.org/mediabunker/lib-nodejs-mbmailer.git#[version tag]
* See the ./mbmailer/test/ for examples

### Who do I talk to? ###

* Ivo Cazemier

### Issues?

* See Jira