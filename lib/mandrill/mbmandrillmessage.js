/**
 * Created by ivocazemier on 10/08/15.
 *
 * See: https://mandrillapp.com/api/docs/messages.nodejs.html
 *
 */

var util = require('util');
var MBMessage = require('../base/mbmessage').MBMessage;
var MBMandrillRecipient = require('./mbmandrillrecipient').MBMandrillRecipient;
var MBMandrillAttachment = require('./mbmandrillattachment').MBMandrillAttachment;
var _ = require('lodash');

// Constructor
function MBMandrillMessage() {
    MBMessage.call(this);

}

util.inherits(MBMandrillMessage, MBMessage);



/**
 * This adapts the generic properties used in MBMessage to the corresponding mandrill api properties
 */
MBMandrillMessage.prototype.toJSON = function () {

    var message = new Object();

    /**
     * Parsing values to mandrill properties
     */

        // The full HTML content to be sent
    message['html'] = this.html;

    // Optional full text content to be sent
    message['text'] = this.text;

    // The message subject
    message['subject'] = this.subject;

    // The sender email address.
    message['from_email'] = this.fromEmail;

    // Optional from name to be used
    message['from_name'] = this.fromName;


    if (this.recipients.length <= 0) {
        var errorMessage = "Ensure there is at least one recipient";
        throw new Error(errorMessage);
    } else if (this.recipients instanceof Array) {
        var validRecipientType = true;
        this.recipients.forEach(function (recipient) {
            if (validRecipientType && !(recipient instanceof MBMandrillRecipient)) {
                validRecipientType = false;
            }
        });
        if (!validRecipientType) {
            var errorMessage = "Ensure all recipients are of instance MBMandrillRecipient";
            throw new Error(errorMessage);
        }
    }

    // An array of recipient information.
    message['to'] = this.recipients;


    //TODO: Not implemented yet
    //message['headers'] = {"Reply-To": "message.reply@example.com"};

    message['important'] = this.important;

    //TODO: Not implemented yet
    //message['track_opens'] = false;

    //TODO: Not implemented yet
    //message['track_clicks'] = false;

    //TODO: Not implemented yet
    //message['auto_text'] = false;

    //TODO: Not implemented yet
    //message['auto_html'] = false;

    //TODO: Not implemented yet
    //message['inline_css'] = false;

    //TODO: Not implemented yet
    //message['url_strip_qs'] = false;

    //TODO: Not implemented yet
    //message['preserve_recipients'] = false;

    //TODO: Not implemented yet
    //message['view_content_link'] = false;

    //TODO: Not implemented yet
    //message['bcc_address'] = "";


    /**
     * TODO: Not implemented yet
     * A custom domain to use for tracking opens and clicks instead of mandrillapp.com
     */
    //message['tracking_domain'] = "";


    /**
     * TODO: Not implemented yet
     * A custom domain to use for SPF/DKIM signing instead of mandrill (for "via" or "on behalf of" in email clients)
     */
    //message['signing_domain'] = "";


    /**
     * TODO: Not implemented yet
     * A custom domain to use for the messages's return-path
     */
    //message['return_path_domain'] = "";


    /**
     * TODO: Not implemented yet
     * Whether to evaluate merge tags in the message. Will automatically be set to true if either merge_vars or global_merge_vars are provided.
     */
    //message['merge'] = false;


    /**
     * TODO: Not implemented yet
     * The merge tag language to use when evaluating merge tags, either mailchimp or handlebars
     * oneof(mailchimp, handlebars)
     */
    //message['merge_language'] = "";


    /**
     * TODO: Not implemented yet
     * Global merge variables to use for all recipients. You can override these per recipient.
     */
    //message['global_merge_vars'] = [{name: "", content: ""}];


    /**
     * TODO: Not implemented yet
     * Per-recipient merge variables, which override global merge variables with the same name.
     */
    //message['merge_vars'] = [{rcpt: "", vars: [{name: "", content: ""}]}, {rcpt: "", vars: [{name: "", content: ""}]}];


    /**
     * TODO: Not implemented yet
     * An array of string to tag the message with.
     * Stats are accumulated using tags, though we only store the first 100 we see, so this should not be unique or change frequently.
     * Tags should be 50 characters or less. Any tags starting with an underscore are reserved for internal use and will cause errors.
     */
    //message['tags'] = ["",""];

    /**
     * TODO: Not implemented yet
     * The unique id of a sub account for this message - must already exist or will fail with an error
     */
    //message['subaccount'] = "";


    /**
     * TODO: Not implemented yet
     * An array of strings indicating for which any matching URLs will automatically have Google Analytics parameters
     * appended to their query string automatically.
     */
    //message['google_analytics_domains'] = [""];

    /**
     * TODO: Not implemented yet
     * Optional string indicating the value to set for the utm_campaign tracking parameter.
     * If this isn't provided the email's from address will be used instead.
     */
    //message['google_analytics_campaign'] = [""] | "" ;


    /**
     * TODO: Not implemented yet
     * Metadata an associative array of user metadata.
     * Mandrill will store this metadata and make it available for retrieval.
     * In addition, you can select up to 10 metadata fields to index and make searchable using the Mandrill search api.
     */
    //message['metadata'] = [""];


    /**
     * TODO: Not implemented yet
     * Per-recipient metadata that will override the global values specified in the metadata parameter.
     */
    //message['recipient_metadata'] = [{rcpt: "", values: [3, 4, 2, 5]}];


    /**
     * FIXME: Highest priority!
     * An array of supported attachments to add to the message
     */

    if (this.attachments instanceof Array) {
        var validAttachmentType = true;
        this.attachments.forEach(function (attachment) {
            if (validAttachmentType && !(attachment instanceof MBMandrillAttachment)) {
                validAttachmentType = true;
            }
        });
        if (!validAttachmentType) {
            var errorMessage = "Ensure all attachments are of instance MBMandrillAttachment";
            throw new Error(errorMessage);
        }
    }
    message['attachments'] = this.attachments;

    /**
     * TODO: Not implemented yet
     * An array of embedded images to add to the message
     */
    //message['images'] = [{type: "", name: "", content: ""}];

    return message;
};


module.exports = {
    MBMandrillMessage: MBMandrillMessage
};