/**
 * Created by ivocazemier on 11/08/15.
 */

// Native
var util = require('util');
var path = require('path');
var http = require('http');
var url = require("url");

// Third party
var async = require('async');
var _ = require('lodash');
var request = require('request');
var fileType = require('file-type');
var isSvg = require('is-svg');

// Own
var MBAttachment = require('../base/mbattachment').MBAttachment;
var SOURCE_TYPES = MBAttachment.SOURCE_TYPES;


function MBMandrillAttachment() {
    MBAttachment.call(this);


}

util.inherits(MBMandrillAttachment, MBAttachment);

MBMandrillAttachment.SOURCE_TYPES = MBAttachment.SOURCE_TYPES;


MBMandrillAttachment.prototype.fetchObjectFromUrlRequest = function (callback) {

    var self = this;
    var requestOptions = {
        gzip: true,
        method: 'GET',
        uri: this.source,
        encoding: null, // To ensure we get the raw binary data
        followRedirect: true,
        maxRedirects: 5
    };


    request(requestOptions, function (error, response, body) {

        /**
         * body is the decompressed response body
         */

        if (error) {
            return callback(error);
        } else {
            var statusCode = response.statusCode;

            if (statusCode == 200) {

                // Fetch the type here, from the response:
                self.sourceContentType = response.headers["content-type"];
                self.sourceContentLength = response.headers["content-length"];

                // If user already set the fileName, we can fetch it from the header response, if there is any
                if (!self.fileName) {
                    self.fileName = response.headers["filename"] || "NoName";
                }

                //console.log("FILE CONTENT TYPE FROM HEADER:" + self.sourceContentType + " length:" + self.sourceContentLength);

                return callback(null, new Buffer(body));
            } else {
                return callback(new Error("Download failed with status code:" + statusCode));
            }
        }


    }).on('data', function (chunk) {

        /**
         * decompressed data as it is received, fetch mime and extension from binary chunk
         */

        if (!self.typeMime) {
            try {
                self.determineFileTypeFromBuffer(chunk);
            } catch (error) {
                console.log("error: could not determine file type, yet!");
            }
        }


    }).on('response', function (response) {
        /**
         * unmodified http.IncomingMessage object
         */
        response.on('data', function (data) {
            // compressed data as it is received
            //console.log('received ' + data.length + ' bytes of compressed data')
        });
    });


};

MBMandrillAttachment.prototype.normalizeFileName = function (callback) {

    var self = this;

    // Ensure we've got a fileName for the Mandrill api
    // When set already, we've got to try to fix/validate/normalize the filename
    if (_.isString(self.fileName) && self.fileName.length > 0) {

        // User set the file name, we need to check if correct
        //var parsedFileName = path.parse(self.fileName);

        var parsedFileExtension = path.extname(self.fileName);
        parsedFileExtension = (parsedFileExtension === ".") ? null : parsedFileExtension;
        var parsedFileBaseName = path.basename(self.fileName, parsedFileExtension);

        // See if user added an extension (name should be there):
        if (_.isString(parsedFileExtension)) {

            // See if we detected an extension from binary data:
            if (self.typeExtension !== undefined) {

                // User set the extension wrong! Fix it with the binary found extension:
                if (parsedFileExtension != self.typeExtension) {
                    //console.log("User set extension to:'" + parsedFileExtension + "' but binary file states:'" + self.typeExtension + "'");

                    self.fileName = parsedFileBaseName + self.typeExtension;
                }

            }

        } else {
            //User did not add an extension, we fix it!
            if (self.typeExtension !== undefined && parsedFileBaseName) {
                self.fileName = parsedFileBaseName + self.typeExtension;
            }
        }
    } else {
        // User forgot to set fileName, we fix it, (default filename)!
        if (self.typeExtension !== undefined) {
            self.fileName = "Attachment" + self.typeExtension;
        } else {
            return callback(new Error("Whoops, did not manage to add the file extension...?"));
        }
    }

    return callback();
};

MBMandrillAttachment.prototype.fetchAttachment = function (callback) {

    var self = this;

    switch (this.sourceType) {
        case MBMandrillAttachment.SOURCE_TYPES.URL_REQUEST:

            self.fetchObjectFromUrlRequest(function (error, bufferInstance) {


                async.series([

                    function (callback) {

                        self.convertSourceBufferToBase64String(bufferInstance, callback);
                    },
                    function (callback) {

                        self.normalizeFileName(callback);

                    }

                ], function (error) {

                    return callback(error);

                });

            });
            break;

        case MBMandrillAttachment.SOURCE_TYPES.BUFFER_TO_BASE64_STRING:


            async.series([

                function (callback) {

                    // The source should contain an instance of Buffer from here:
                    self.determineFileTypeFromBuffer(self.source, callback);

                },
                function (callback) {

                    self.convertSourceBufferToBase64String(self.source, callback);
                },
                function (callback) {

                    self.normalizeFileName(callback);

                }

            ], function (error) {

                return callback(error);

            });


            break;
        default:
            return callback(new Error("Unhandled source type"));
    }
};

/**
 * This method looks in the binary data (Buffer) and determines the properties:
 * this.typeMime
 * this.typeExtension
 * @param callback
 * @returns {*}
 */
MBMandrillAttachment.prototype.determineFileTypeFromBuffer = function (buffer, callback) {

    var self = this;

    if (buffer instanceof Buffer) {

        var type = fileType(buffer);
        if (type) {

            self.typeMime = type.mime;
            self.typeExtension = (_.isString(type.ext) && type.ext.length > 0) ? ("." + type.ext) : undefined;

            // Successfully found a file type:
            if (callback !== undefined) {
                return callback(null);
            }

        } else {

            // Could not find a file type, could still be a svg:
            var svgFound = isSvg(buffer);
            if (svgFound) {
                self.typeExtension = '.svg';
                self.typeMime = 'image/svg+xml';
                if (callback !== undefined) {
                    return callback(null);
                }

            } else {
                var error = new Error("Could not determine file type from binary data in buffer");
                if (callback !== undefined) {
                    // Could not determine file type, even not a svg!
                    return callback(error);
                } else {
                    throw error;
                }
            }
        }

    } else {

        var error = new Error("buffer should contain an instance of Buffer");
        if (callback !== undefined) {
            return callback(error);
        } else {
            throw error;
        }
    }

};

MBMandrillAttachment.prototype.convertSourceBufferToBase64String = function (buffer, callback) {

    var self = this;

    if (buffer instanceof Buffer) {

        var base64String = buffer.toString('base64');
        if (_.isString(base64String) && base64String.length > 0) {

            // This sets the base64 content string for the Mandrill api call:
            self.content = base64String;
            return callback();
        } else {
            return callback(new Error("buffer empty?"));
        }

    } else {
        return callback(new Error("source should contain an instance of Buffer"));
    }

};


/**
 * This ensures the generic properties are mapped to the Mandrill api fields
 * @returns {Object}
 */
MBMandrillAttachment.prototype.toJSON = function () {

    var attachmentObject = new Object();

    // The MIME type of the attachment
    attachmentObject['type'] = this.typeMime;

    // The file name of the attachment
    attachmentObject['name'] = this.fileName;

    // The content of the attachment as a base64-encoded string
    if (this.content) {
        attachmentObject['content'] = this.content;
    } else {
        var errorMessage = "For some reason, content property not set (needs a base64-encoded string)";
        throw new Error(errorMessage);
    }

    return attachmentObject;

};


module.exports = {
    MBMandrillAttachment: MBMandrillAttachment
};

