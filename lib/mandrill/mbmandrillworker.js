/**
 * Created by ivocazemier on 07/08/15.
 */



var util = require('util');
var path = require('path');

var _ = require('lodash');
var async = require('async');

var mandrill = require('mandrill-api/mandrill');
var mbMailerConfig = require('../mbmailerconfig');

var MBMandrillMessage = require('./mbmandrillmessage').MBMandrillMessage;


function MBMandrillWorkerError(message) {
    Error.call(this);
    this.message = message;
}

util.inherits(MBMandrillWorkerError, Error);

/**
 * Default mandrill settings
 */
var defaultMandrillSettings = {
    apiKey: process.env.MANDRILL_APIKEY || "SET KEY AS ENVIRONMENT VARIABLE, PLEASE.",

    /**
     * Enable a background sending mode that is optimized for bulk sending.
     * In async mode, messages/send will immediately return a status of "queued" for every recipient.
     * To handle rejections when sending in async mode, set up a webhook for the 'reject' event.
     * Defaults to false for messages with no more than 10 recipients; messages with more than 10 recipients are
     * always sent asynchronously, regardless of the value of async.
     */
    async: false,

    /**
     * The name of the dedicated ip pool that should be used to send the message.
     * If you do not have any dedicated IPs, this parameter has no effect.
     * If you specify a pool that does not exist, your default pool will be used instead.
     */
    ip_pool: "Main Pool"

};


function MBMandrillWorker() {
    this.mandrillClient = null;
}

MBMandrillWorker.prototype.initialize = function (settings, callback) {

    // Merge with default mandrill settings (we expect a mandrill property)
    if (settings !== undefined) {
        this.settings = _.merge(defaultMandrillSettings, settings);
    } else {
        this.settings = defaultMandrillSettings;
    }

    if (_.isString(this.settings.apiKey)) {

        // Initialize client
        this.mandrillClient = new mandrill.Mandrill(this.settings.apiKey);

        // Mandrill client initialized!
        return callback(null, "MBMandrillWorker initialized!");

    } else {
        var errorMessage = "Could not initialize mandrill (no api key found)";
        var error = new Error(errorMessage);
        return callback(error);
    }
};


MBMandrillWorker.prototype.degradeWorkerGracefully = function (callback) {
    // Nothing to do, yet!
    return callback();
};


MBMandrillWorker.prototype.executeSending = function (mbMessage, callback) {

    var self = this;
    var ipPool = this.settings.ip_pool;
    var sendAsync = this.settings.async;

    self.mandrillClient.messages.send({
            "message": mbMessage,
            "async": sendAsync,
            "ip_pool": ipPool,
            "send_at": null //TODO: Not implemented, needs "Email scheduling is only available for accounts with a positive balance"
        },
        function (result) {

            return callback(null, result);

        },
        function (mandrillError) {
            // Mandrill returns the error as an object with name and message keys
            // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'

            switch (mandrillError.name) {
                case 'Invalid_Key':
                    break;
                case 'Unknown_Message':
                    break;
                case 'ValidationError':
                    break;
                case 'GeneralError':
                    break;
                default:
            }

            var errorMessage = "Mandrill failed with message:'" + mandrillError.message + "'";
            var workerError = new MBMandrillWorkerError(errorMessage);
            return callback(workerError);
        });


};

MBMandrillWorker.prototype.sendMailWithMessage = function (mbMessage, callback) {

    var self = this;

    if (mbMessage instanceof MBMandrillMessage) {

        var attachments = mbMessage.attachments;

        if (attachments instanceof Array && attachments.length > 0) {
            async.eachSeries(attachments, function (attachment, callback) {

                // This method, will look at the type of attachment (default: Fetches from url as source),
                // and ensure this is converted to a base64 string for mandrill
                attachment.fetchAttachment(callback);


            }, function (error) {

                if (error) {
                    var errorMessage = "Failed in fetching resource with message:" + error.message;
                    var workerError = new MBMandrillWorkerError(errorMessage);
                    return callback(workerError);
                } else {
                    // Converted all attachments to base64 strings, carry on in sending
                    self.executeSending(mbMessage, callback);
                }

            });
        } else {
            // No attachments, send 'immediately'
            self.executeSending(mbMessage, callback);
        }


    } else {
        var errorMessage = "mbMessage not of instance MBMandrillMessage";
        var workerError = new MBMandrillWorkerError(errorMessage);
        return callback(workerError);
    }
};


module.exports = {
    MBMandrillWorker: MBMandrillWorker
};
