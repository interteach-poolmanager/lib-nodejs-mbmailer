/**
 * Created by ivocazemier on 10/08/15.
 */

var util = require('util');
var MBRecipient = require('../base/mbrecipient').MBRecipient;

function MBMandrillRecipient() {


}

util.inherits(MBMandrillRecipient, MBRecipient);


/**
 *
 * This adapts the MBMandrillRecipient instance to the api json signature which is mandatory for
 * the mandrill-api message.
 *
 * This is normally called by the JSON.stringify(object)
 *
 * @returns {Object}
 */
MBMandrillRecipient.prototype.toJSON = function () {



    var recipient = new Object();

    // The email address of the recipient
    recipient['email'] = this.email;

    // The optional display name to use for the recipient
    recipient['name'] = this.name;

    // The header type to use for the recipient, defaults to "to" if not provided
    // oneof(to, cc, bcc)
    switch (this.type) {
        case MBMandrillRecipient.RECIPIENT_TYPES.TO:

            recipient['type'] = 'to';

            break;
        case MBMandrillRecipient.RECIPIENT_TYPES.CC:

            recipient['type'] = 'cc';

            break;
        case MBMandrillRecipient.RECIPIENT_TYPES.BCC:

            recipient['type'] = 'bcc';

            break;
        default:
            recipient['type'] = 'to';
    }

    return recipient;

};


MBMandrillRecipient.RECIPIENT_TYPES = MBRecipient.RECIPIENT_TYPES;

module.exports = {
    MBMandrillRecipient: MBMandrillRecipient
};