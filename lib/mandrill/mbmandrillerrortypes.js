/**
 * Created by ivocazemier on 11/08/15.
 */


var MANDRILL_ERROR_TYPES = {
    INVALID_KEY: 'Invalid_Key',
    PAYMENT_REQUIRED: 'PaymentRequired',
    UNKNOWN_SUBACCOUNT: 'Unknown_Subaccount',
    VALIDATION_ERROR: 'ValidationError',
    GENERAL_ERROR: 'GeneralError'
};


module.exports = {

    MANDRILL_ERROR_TYPES:MANDRILL_ERROR_TYPES

};