/**
 * Created by ivocazemier on 10/08/15.
 */

//Native:
var util = require('util');
var async = require('async');
var EventEmitter = require('events').EventEmitter;
var MBAttachment = require('./base/mbattachment').MBAttachment;

//Third Party:


//Own:
var MBMessage = require('./base/mbmessage').MBMessage;


var QUEUE_EVENTS = {
    ENQUEUE_MESSAGE: 'enqueue_message'
};


function MBMailerError() {
    Error.call(this);
}

util.inherits(MBMailerError, Error);

// Constructor
function MBMailer() {


}
util.inherits(MBMailer, EventEmitter);

/**
 * The message will be queued eventually
 * Before we queue the message, the content is checked on:
 * - Consistency of the attachments (if available online)
 *
 *
 * @param message
 * @param callback
 * @returns {*}
 */
MBMailer.prototype.sendMailWithMessage = function (message, callback) {

    var self = this;

    if (message instanceof MBMessage) {


        // Before we queue the message, we need to check the urls for the attachments, all resources need to be available online
        if (message.attachments instanceof Array && message.checkOnConsitency) {

            async.eachSeries(message.attachments, function (attachement, callback) {

                if (attachement instanceof MBAttachment) {

                    attachement.checkConsistencyBeforeQueueing(callback);

                } else {
                    var errorMessage = "attachment not of instance MBAttachment";
                    var error = new MBMailerError(errorMessage);
                    return callback(error);
                }
            }, function (error) {

                if (error) {

                    return callback(error);

                } else {
                    //TODO: Later we will implement a queuing system (e.g. Kue), for now just with native events.
                    self.emit(QUEUE_EVENTS.ENQUEUE_MESSAGE, message);
                    return callback(null, message);
                }

            });

        } else {

            //TODO: Later we will implement a queuing system (e.g. Kue), for now just with native events.
            self.emit(QUEUE_EVENTS.ENQUEUE_MESSAGE, message);
            return callback(null, message);

        }

    } else {

        var errorMessage = "message needs to be an instance of MBMessage";
        var error = new MBMailerError(errorMessage);
        return callback(error);

    }


};



module.exports ={
    MBMailer:MBMailer,
    QUEUE_EVENTS:QUEUE_EVENTS
};
