/**
 * Created by ivocazemier on 10/08/15.
 */

var _ = require('lodash');
var path = require('path');


var defaultSettings = {

};
var settings = null;

module.exports = {

    loadConfig: function (appRootPath, callback) {

        settings = {};


        // Resolve path to the settings (overriding the default settings above)

        var pathToMailerConfigFile = path.normalize(appRootPath + "/config/mailer.js");
        var mailerSettingsPath = path.resolve(pathToMailerConfigFile);


        var configFileBaseName = path.basename(mailerSettingsPath, ".js");
        var configFileExtension = path.extname(mailerSettingsPath);


        if (mailerSettingsPath && configFileBaseName === 'mailer' && configFileExtension === '.js') {

            var mailerSettings = require(mailerSettingsPath);
            settings = _.merge(defaultSettings, mailerSettings)

        } else {
            settings = defaultSettings;
        }


        return callback(null, settings);

    }


};
