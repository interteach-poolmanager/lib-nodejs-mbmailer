/**
 * Created by ivocazemier on 10/08/15.
 */



function MBMessage() {

    if (this.constructor === MBMessage) {
        throw new Error("Can't instantiate abstract class!");
    }

    this.html = null;
    this.text = null;
    this.subject = null;
    this.fromEmail = null;
    this.fromName = null;
    this.recipients = null;
    this.replyTo = null;
    this.important = false;
    this.attachments = null;
    this.checkOnConsitency = true;

}


module.exports = {

    MBMessage: MBMessage

};