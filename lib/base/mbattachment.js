/**
 * Created by ivocazemier on 11/08/15.
 */

var util = require('util');
var url = require("url");
var request = require('request');
var path = require('path');
var fs = require('fs');

var SOURCE_TYPES = {
    URL_REQUEST: 'URL_REQUEST',
    BUFFER_TO_BASE64_STRING:'BUFFER_TO_BASE64_STRING',
    FILE_PATH:'FILE_PATH'
};


function MBAttachmentError(message) {
    Error.call(this);

    this.message = message;

}

util.inherits(MBAttachmentError, Error);

MBAttachment.SOURCE_TYPES = SOURCE_TYPES;

function MBAttachment() {
    this.typeMime = null;
    this.typeExtension = null;
    this.fileName = null;
    this.content = null;

    this.source = null;
    this.sourceConsistent = false;
    this.sourceType = SOURCE_TYPES.URL_REQUEST;
    this.sourceContentType = null;
    this.sourceContentLength = null;
}


MBAttachment.prototype.checkConsistencyBeforeQueueing = function (callback) {

    var self = this;

    switch (this.sourceType) {

        case MBAttachment.SOURCE_TYPES.URL_REQUEST:

            // We only do a 'HEAD' request, to see if the server responds with 200
            // (gives a good indication about the availability of the resource)

            var options = {
                method: 'HEAD',
                uri: this.source,
                gzip: true,
                encoding: null,
                followRedirect: true,
                maxRedirects: 5
            };

            // Execute the request:
            request(options, function (error, response, body) {

                if (error) {
                    var errorMessage = "Something went wrong with requesting a resource with error message:" + error.message;
                    var attachmentError = new MBAttachmentError(errorMessage);

                    return callback(attachmentError);
                }

                //console.log(JSON.stringify(response.headers));
                
                if (response.statusCode !== undefined && response.statusCode != 200) {

                    var errorMessage = "Consistency check failed with http status code:" + response.statusCode;
                    var error = new MBAttachmentError(errorMessage);
                    return callback(error);

                }

                // No error !
                return callback(null);


            });


            break;
        case MBAttachment.SOURCE_TYPES.BUFFER_TO_BASE64_STRING:
            return callback();
            break;
        case MBAttachment.SOURCE_TYPES.FILE_PATH:

            fs.exists(this.source, function(exists) {
                if(exists) {
                    return callback(null);
                } else {
                    var errorMessage = "Consistency check failed, attachment file not found";
                    var error = new MBAttachmentError(errorMessage);
                    return callback(error);
                }
            });
            break;
        default:
            var errorMessage = "Unhandled source type";
            var error = new MBAttachmentError(errorMessage);
            return callback(error);
    }
};




module.exports = {
    MBAttachment: MBAttachment,
    MBAttachmentError: MBAttachmentError
};

