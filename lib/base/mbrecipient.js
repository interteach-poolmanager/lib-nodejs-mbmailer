/**
 * Created by ivocazemier on 10/08/15.
 */


var RECIPIENT_TYPES = {
    TO: 'TO',
    CC: 'CC',
    BCC: 'BCC'
};

function MBRecipient (){

    if (this.constructor === MBRecipient) {
        throw new Error("Can't instantiate abstract class!");
    }

    this.email = null;
    this.name = null;
    this.type = null;
}

MBRecipient.RECIPIENT_TYPES = RECIPIENT_TYPES;

module.exports = {

    MBRecipient:MBRecipient

};