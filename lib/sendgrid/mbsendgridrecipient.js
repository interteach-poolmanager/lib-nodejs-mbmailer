/**
 * Created by ivocazemier on 10/08/15.
 */

var util = require('util');
var MBRecipient = require('../base/mbrecipient').MBRecipient;

function MBSendGridRecipient() {


}

util.inherits(MBSendGridRecipient, MBRecipient);


MBSendGridRecipient.RECIPIENT_TYPES = MBRecipient.RECIPIENT_TYPES;

module.exports = {
    MBSendGridRecipient: MBSendGridRecipient
};