/**
 * Created by ivocazemier on 10/08/15.
 *
 * See: https://mandrillapp.com/api/docs/messages.nodejs.html
 *
 */

var util = require('util');
var MBMessage = require('../base/mbmessage').MBMessage;
var MBSendGridRecipient = require('./mbsendgridrecipient').MBSendGridRecipient;
var MBSendGridAttachment = require('./mbsendgridattachment').MBSendGridAttachment;
var _ = require('lodash');

// Constructor
function MBSendGridMessage() {
    MBMessage.call(this);

}

util.inherits(MBSendGridMessage, MBMessage);



/**
 * This adapts the generic properties used in MBMessage to the corresponding SendGrid api properties
 */
MBSendGridMessage.prototype.toJSON = function () {

    var message = new Object();

        // The full HTML content to be sent
    message['html'] = this.html;

    // Optional full text content to be sent
    message['text'] = this.text;

    // The message subject
    message['subject'] = this.subject;

    // The sender email address.
    message['from'] = this.fromEmail;

    // Optional from name to be used
    message['fromname'] = this.fromName;
    message['to'] = [];
    message['toname'] = [];
    message['cc'] = [];
    message['bcc'] = [];

    if (this.recipients.length <= 0) {
        var errorMessage = "Ensure there is at least one recipient";
        throw new Error(errorMessage);
    } else if (this.recipients instanceof Array) {
        var validRecipientType = true;
        this.recipients.forEach(function (recipient) {
            if (validRecipientType && recipient instanceof MBSendGridRecipient) {

                switch(recipient.type) {
                    case MBSendGridRecipient.RECIPIENT_TYPES.TO:
                        message['to'].push(recipient.email);
                        message['toname'].push(recipient.name);
                        break;
                    case MBSendGridRecipient.RECIPIENT_TYPES.CC:
                        message['cc'].push(recipient.email);
                        break;
                    case MBSendGridRecipient.RECIPIENT_TYPES.BCC:
                        message['bcc'].push(recipient.email);
                        break;
                }
            } else {
                validAttachmentType = false;
            }
        });
        if (!validRecipientType) {
            var errorMessage = "Ensure all recipients are of instance MBSendGridRecipient";
            throw new Error(errorMessage);
        }
    }
    //TODO: Not implemented yet
    //message['replyto'] = "";

    //TODO: Not implemented yet
    //message['date'] = "";

    message['files'] = [];


    if (this.attachments instanceof Array) {
        var validAttachmentType = true;
        this.attachments.forEach(function (attachment) {
            if (validAttachmentType && attachment instanceof MBSendGridAttachment) {
                message['files'].push( attachment.toJSON());
            } else {
                validAttachmentType = false;
            }
        });
        if (!validAttachmentType) {
            var errorMessage = "Ensure all attachments are of instance MBSendGridAttachment";
            throw new Error(errorMessage);
        }
    }

    return message;
};

module.exports = {
    MBSendGridMessage: MBSendGridMessage
};