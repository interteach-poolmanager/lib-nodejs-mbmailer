/**
 * Created by ivocazemier on 07/08/15.
 */



var util = require('util');
var path = require('path');

var _ = require('lodash');
var async = require('async');

var SendGrid = require('sendgrid');
var mbMailerConfig = require('../mbmailerconfig');

var MBSendGridMessage = require('./mbsendgridmessage').MBSendGridMessage;
var MBSendGridAttachment = require('./mbsendgridattachment').MBSendGridAttachment;

function MBSendGridWorkerError(message) {
    Error.call(this);
    this.message = message;
}

util.inherits(MBSendGridWorkerError, Error);

/**
 * Default sendgrid settings (authentication is done by username and password or by api key, only one of both authentication methods is needed)
 * userName: sendgrid account username
 * password: sendgrid account password
 * apiKey: sendgrid api key
 */
var defaultSendGridSettings = {
    userName: process.env.SENDGRID_USERNAME || null,
    password: process.env.SENDGRID_PASSWORD || null,
    apiKey: process.env.SENDGRID_APIKEY || null
};

function MBSendGridWorker() {
    this.sendgridClient = null;
}

MBSendGridWorker.prototype.initialize = function (settings, callback) {
    // Merge with default sendgrid settings (we expect a sendgrid property)
    if (settings !== undefined) {
        this.settings = _.merge(defaultSendGridSettings, settings);
    } else {
        this.settings = defaultSendGridSettings;
    }

    //Sendgrid is connected by username and password or by accesskey
    if (_.isString(this.settings.userName) && _.isString(this.settings.password) || _.isString(this.settings.apiKey)) {
        if(this.settings.userName && this.settings.password) {
            this.sendgridClient = new SendGrid(this.settings.userName, this.settings.password);
        } else if(this.settings.apiKey) {
            this.sendgridClient = new SendGrid(this.settings.apiKey);
        }

        if(this.sendgridClient) {
            // SendGrid client initialized!
            return callback(null, "MBSendGridWorker initialized!");
        } else {
            var errorMessage = "Could not initialize sendgrid";
            var error = new Error(errorMessage);
            return callback(error);
        }
    } else {
        var errorMessage = "Could not initialize sendgrid (no username and password or access key not found)";
        var error = new Error(errorMessage);
        return callback(error);
    }

};


MBSendGridWorker.prototype.degradeWorkerGracefully = function (callback) {

    // Nothing to do, yet!
    return callback();

};

MBSendGridWorker.prototype.executeSending = function (mbMessage, callback) {
    var self = this;

    var message = mbMessage.toJSON();

    message['smtpapi'] =  new self.sendgridClient.smtpapi();

    self.sendgridClient.send(message, function(sendgridError, json) {
        var workerError = null;

        if(sendgridError) {
            var errorMessage = "SendGrid failed with message:'" + sendgridError.message + "'";
            workerError = new MBSendGridWorkerError(errorMessage);
        }
        return callback(workerError);
    });
};

MBSendGridWorker.prototype.sendMailWithMessage = function (mbMessage, callback) {

    var self = this;

    if (mbMessage instanceof MBSendGridMessage) {

        if (mbMessage.attachments instanceof Array) {

            async.each(mbMessage.attachments, function(attachment, callback) {
                if (attachment instanceof MBSendGridAttachment) {
                    attachment.prepareAttachment(function(error) {
                        return callback(error);
                    })
                } else {
                    return callback(new Error("Invalid attachment type"));
                };
            }, function(error) {
                if(!error) {
                    self.executeSending(mbMessage, callback);
                } else {
                    var errorMessage = "Error handling attachments";
                    var workerError = new MBSendGridWorkerError(errorMessage);
                    return callback(workerError);
                }
            });
        } else {
            self.executeSending(mbMessage, callback);
        }
    } else {
        var errorMessage = "mbMessage not of instance MBSendGridMessage";
        var workerError = new MBSendGridWorkerError(errorMessage);
        return callback(workerError);
    }
};


module.exports = {
    MBSendGridWorker: MBSendGridWorker
};
