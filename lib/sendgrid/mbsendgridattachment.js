/**
 * Created by ivocazemier on 11/08/15.
 */

var util = require('util');
var path = require('path');
var request = require('request');
var url = require("url");
var _ = require('lodash');
var fileType = require('file-type');
var isSvg = require('is-svg');

var MBAttachment = require('../base/mbattachment').MBAttachment;

function MBSendGridAttachment() {
    MBAttachment.call(this);
}

util.inherits(MBSendGridAttachment, MBAttachment);

MBSendGridAttachment.SOURCE_TYPES = MBAttachment.SOURCE_TYPES;

/**
 * Prepare the attachment for url data
 * @param callback
 */
MBSendGridAttachment.prototype.prepareContentFromUrl = function (callback) {
    var self = this;
    var requestOptions = {
        gzip: true,
        method: 'GET',
        uri: this.source,
        encoding: null, // To ensure we get the raw binary data
        followRedirect: true,
        maxRedirects: 5
    };

    request(requestOptions, function (error, response, body) {
        /**
         * body is the decompressed response body
         */

        if (error) {
            return callback(error);
        } else {
            var statusCode = response.statusCode;

            if (statusCode == 200) {

                // Fetch the type here, from the response:
                self.sourceContentType = response.headers["content-type"];
                self.sourceContentLength = response.headers["content-length"];

                // If user already set the fileName, we can fetch it from the header response, if there is any
                if (!self.fileName) {
                    self.fileName = response.headers["filename"] || "NoName";
                }

                self.sourceType = MBSendGridAttachment.SOURCE_TYPES.BUFFER_TO_BASE64_STRING;
                self.source = new Buffer(body);

                return callback(null);
            } else {
                return callback(new Error("Download failed with status code:" + statusCode));
            }
        }


    }).on('data', function (chunk) {
        /**
         * decompressed data as it is received, fetch mime and extension from binary chunk
         */

        if (!self.typeMime) {
            try {
                self.prepareAttachmentFromBuffer(chunk);
            } catch (error) {
                console.log("error: could not determine file type, yet!");
            }
        }
    }).on('response', function (response) {
        response.on('data', function (data) {

        });
    });
};

/**
 * This method looks in the binary data (Buffer) and determines the properties:
 * @param buffer
 * @param callback
 * @returns {*}
 */
MBSendGridAttachment.prototype.prepareAttachmentFromBuffer = function (buffer, callback) {

    var self = this;

    if (buffer instanceof Buffer) {

        var type = fileType(buffer);
        if (type) {

            self.typeMime = type.mime;
            self.typeExtension = (_.isString(type.ext) && type.ext.length > 0) ? ("." + type.ext) : undefined;

            // Successfully found a file type:
            if (callback !== undefined) {
                return callback(null);
            }

        } else {

            // Could not find a file type, could still be a svg:
            var svgFound = isSvg(buffer);
            if (svgFound) {
                self.typeExtension = '.svg';
                self.typeMime = 'image/svg+xml';
                if (callback !== undefined) {
                    return callback(null);
                }

            } else {
                var error = new Error("Could not determine file type from binary data in buffer");
                if (callback !== undefined) {
                    // Could not determine file type, even not a svg!
                    return callback(error);
                } else {
                    throw error;
                }
            }
        }

    } else {

        var error = new Error("buffer should contain an instance of Buffer");
        if (callback !== undefined) {
            return callback(error);
        } else {
            throw error;
        }
    }
};

/**
 * Correct the file extension if needed
 * @param callback
 * @returns {*}
 */
MBSendGridAttachment.prototype.normalizeFileName = function (callback) {

    var self = this;

    // Ensure we've got a fileName for the Mandrill api
    // When set already, we've got to try to fix/validate/normalize the filename
    if (_.isString(self.fileName) && self.fileName.length > 0) {

        // User set the file name, we need to check if correct
        //var parsedFileName = path.parse(self.fileName);

        var parsedFileExtension = path.extname(self.fileName);
        parsedFileExtension = (parsedFileExtension === ".") ? null : parsedFileExtension;
        var parsedFileBaseName = path.basename(self.fileName, parsedFileExtension);

        // See if user added an extension (name should be there):
        if (_.isString(parsedFileExtension)) {

            // See if we detected an extension from binary data:
            if (self.typeExtension !== undefined) {

                // User set the extension wrong! Fix it with the binary found extension:
                if (parsedFileExtension != self.typeExtension) {
                    self.fileName = parsedFileBaseName + self.typeExtension;
                }
            }
        } else {
            //User did not add an extension, we fix it!
            if (self.typeExtension !== undefined && parsedFileBaseName) {
                self.fileName = parsedFileBaseName + self.typeExtension;
            }
        }
    } else {
        // User forgot to set fileName, we fix it, (default filename)!
        if (self.typeExtension !== undefined) {
            self.fileName = "Attachment" + self.typeExtension;
        } else {
            return callback(new Error("Whoops, did not manage to add the file extension...?"));
        }
    }

    return callback();
};

/**
 * Prepare the attachment for sending
 * @param callback
 * @returns {*}
 */
MBSendGridAttachment.prototype.prepareAttachment = function (callback) {

    var self = this;

    switch (this.sourceType) {
        case MBSendGridAttachment.SOURCE_TYPES.URL_REQUEST:

            self.prepareContentFromUrl(function (error) {
                if(!error) {
                    self.normalizeFileName(function(error) {
                        return callback(error);
                    });
                } else {
                    return callback(error);
                }
            });
            break;
        case MBSendGridAttachment.SOURCE_TYPES.BUFFER_TO_BASE64_STRING:
            self.prepareAttachmentFromBuffer(self.source, function(error) {
                if(!error) {
                    self.normalizeFileName(function(error) {
                        return callback(error);
                    });
                } else {
                    return callback(error);
                }
            });
            break;
        case MBSendGridAttachment.SOURCE_TYPES.FILE_PATH:
            //This will be handled by sendgrid
            return callback(null);
            break;
        default:
            return callback(new Error("Unhandled source type"));
    }
};


/**
 * This ensures the generic properties are mapped to the SendGrid api fields
 * @returns {Object}
 */
MBSendGridAttachment.prototype.toJSON = function () {

    var attachmentObject = new Object();

    // The MIME type of the attachment
    attachmentObject['contentType'] = this.sourceContentType;
    attachmentObject['filename'] = this.fileName;
    attachmentObject['cid'] = '';
    attachmentObject['path'] = '';
    attachmentObject['url'] = '';
    attachmentObject['content'] = null;

    switch(this.sourceType) {
        case MBAttachment.SOURCE_TYPES.URL_REQUEST:
            attachmentObject['url'] = this.source;
            break;
        case MBAttachment.SOURCE_TYPES.BUFFER_TO_BASE64_STRING:
            attachmentObject['content'] = this.source;
            break;
        case MBAttachment.SOURCE_TYPES.FILE_PATH:
            attachmentObject['path'] = this.source;
            break;
        default:
            break;
    }

    return attachmentObject;
};


module.exports = {
    MBSendGridAttachment: MBSendGridAttachment
};

